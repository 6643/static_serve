part of static_serve;

class Dir {
  final String web_dir;
  Dir(this.web_dir);

  file(HttpRequest request) {
    final String file_path =
        request.uri.path == '/' ? '/index.html' : request.uri.path;

    var file = new File('$web_dir$file_path');

    if (!file.existsSync()) {
      file = new File('$web_dir/index.html');
    }

    final ignore = new File('ignore');
    if (ignore.existsSync() &&
        ignore
            .readAsLinesSync()
            .map((f) => path.canonicalize(f))
            .contains(path.canonicalize(file.absolute.path))) {
      file = new File('$web_dir/index.html');
    }

    var start = 0;
    var end = file.lengthSync();

    final range = request.headers[HttpHeaders.RANGE];
    if (range != null) {
      RegExp exp = new RegExp("(\\d+)");
      var matches = exp.firstMatch(range.first);
      start = int.tryParse(matches.group(1)) ?? end;

      request.response.statusCode = HttpStatus.PARTIAL_CONTENT;
      request.response.headers
          .add(HttpHeaders.CONTENT_RANGE, 'bytes $start-${end-1}/$end');
    }

    request.response.headers.add(HttpHeaders.CONTENT_LENGTH, end - start);

    file.openRead(start, end).pipe(request.response).catchError((e) {});
  }
}
